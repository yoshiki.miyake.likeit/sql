create database rezodb default character set utf8;
use rezodb;
create table item_category(category_id int primary key auto_increment, category_name varchar(256) not NULL);
create table item(item_id int primary key AUTO_INCREMENT, item_name varchar(256) not null, item_price int not null, category_id int);